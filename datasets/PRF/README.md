# Dados de acidentes da PRF

Retirados do _[vias-seguras]_(http://www.vias-seguras.com/dados_da_prf/dados_abertos_de_acidentes_nas_rodovias_federais_ate_2017)

Os sites públicos no geral são uma nojeira e frequentemente escondem a página de acesso aos dados. Acredito que o pessoal do R pegou dessa página, já que esses dados são parecidos com os utilizados em sua apresentação.

Uma alternativa pode ser o site [de divulgação de dados](http://dados.gov.br/dataset/acidentes-rodovias-federais/resource/49c55745-9db8-4e6d-b4b0-8bc008085807?inner_span=True) do próprio governo, mas admito que não chequei ainda.
